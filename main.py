import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost password=12345 user=postgres")

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = (
    "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
)
buy_validate_amount = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
buy_increase_inventory_amount = "UPDATE Inventory SET amount = amount + %(amount)s where USERNAME = %(username)s AND product = %(product)s"


def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Not enough funds on balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            cur.execute(buy_validate_amount, obj)
            current_amount = cur.fetchone()[0]
            if current_amount + amount > 100:
                raise RuntimeError("Cannot buy more than 100")

            cur.execute(buy_increase_inventory_amount, obj)
            conn.commit()


buy_product("Alice", "marshmello", 111)
