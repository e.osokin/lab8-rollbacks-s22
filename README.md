# Lab9 - Rollbacks and reliability




## Inventory








```sql

CREATE TABLE Inventory (
	id serial PRIMARY KEY NOT NULL,
	username TEXT NOT NULL REFERENCES Player (username),
	product text NOT NULL REFERENCES Shop (product),
	amount INT CHECK (amount >= 0),
	UNIQUE (username, product)
);


INSERT INTO Inventory (username, product, amount) VALUES ('Bob', 'marshmello', 0);
INSERT INTO Inventory (username, product, amount) VALUES ('Alice', 'marshmello', 0);
```


```sql
CREATE DATABASE lab;
\c lab
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
```









+ Pay attention to constraints that we have for created table. Player balance, price and amount of product in stock cannot be negative. 
+ Ofcourse for testing we need some sample data, lets fill in tables with it:
```
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO PLayer (username, balance) VALUES ('Bob', 200);

INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```
